package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.semikolenov.tm.api.service.IConnectionService;
import ru.t1.semikolenov.tm.api.service.IPropertyService;
import ru.t1.semikolenov.tm.api.service.dto.IProjectServiceDTO;
import ru.t1.semikolenov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.t1.semikolenov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.semikolenov.tm.api.service.dto.IUserServiceDTO;
import ru.t1.semikolenov.tm.dto.model.ProjectDTO;
import ru.t1.semikolenov.tm.dto.model.TaskDTO;
import ru.t1.semikolenov.tm.dto.model.UserDTO;
import ru.t1.semikolenov.tm.exception.entity.TaskNotFoundException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.exception.field.EmptyUserIdException;
import ru.t1.semikolenov.tm.service.dto.ProjectServiceDTO;
import ru.t1.semikolenov.tm.service.dto.ProjectTaskServiceDTO;
import ru.t1.semikolenov.tm.service.dto.TaskServiceDTO;
import ru.t1.semikolenov.tm.service.dto.UserServiceDTO;

public class ProjectTaskServiceTest {

    @NotNull
    private IProjectTaskServiceDTO projectTaskService;

    @NotNull
    private IUserServiceDTO userService;

    @NotNull
    private IProjectServiceDTO projectService;

    @NotNull
    private ITaskServiceDTO taskService;

    private String USER_ID;

    private String PROJECT_ID;

    private String TASK_ID;

    @Before
    public void init() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        projectTaskService = new ProjectTaskServiceDTO(connectionService);
        userService = new UserServiceDTO(connectionService, propertyService);
        projectService = new ProjectServiceDTO(connectionService);
        taskService = new TaskServiceDTO(connectionService);
        @NotNull final UserDTO user = userService.create("user_test", "user_test");
        USER_ID = user.getId();
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "project_test");
        PROJECT_ID = project.getId();
        @NotNull final TaskDTO task = taskService.create(USER_ID, "task_test");
        TASK_ID = task.getId();
    }

    @After
    public void end() {
        taskService.clear(USER_ID);
        projectService.clear(USER_ID);
        userService.removeByLogin("user_test");
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.bindTaskToProject("", PROJECT_ID, TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, "", TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, "task_id"));
        projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, TASK_ID);
        @NotNull final TaskDTO task = taskService.findOneById(TASK_ID);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(PROJECT_ID, task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.unbindTaskFromProject("", PROJECT_ID, TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, "", TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, "task_id"));
        projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, TASK_ID);
        @NotNull final TaskDTO task_unbind = taskService.findOneById(TASK_ID);
        Assert.assertNull(task_unbind.getProjectId());
    }

}
