package ru.t1.semikolenov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1.semikolenov.tm.comparator.CreatedComparator;
import ru.t1.semikolenov.tm.comparator.DateBeginComparator;
import ru.t1.semikolenov.tm.comparator.StatusComparator;
import ru.t1.semikolenov.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;

public abstract class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends AbstractRepositoryDTO<M>
        implements IUserOwnedRepositoryDTO<M> {

    public AbstractUserOwnedRepositoryDTO(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateBeginComparator.INSTANCE) return "status";
        else return "name";
    }

    public void add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUserId(userId);
        entityManager.persist(model);
    }

    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUserId(userId);
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        model.setUserId(userId);
        entityManager.remove(model);
    }

}
