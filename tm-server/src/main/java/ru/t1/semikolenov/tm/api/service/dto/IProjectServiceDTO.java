package ru.t1.semikolenov.tm.api.service.dto;

import ru.t1.semikolenov.tm.dto.model.ProjectDTO;

public interface IProjectServiceDTO extends IUserOwnedServiceDTO<ProjectDTO> {
}
