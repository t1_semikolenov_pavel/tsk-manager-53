package ru.t1.semikolenov.tm.api.service.dto;

import ru.t1.semikolenov.tm.api.repository.dto.IRepositoryDTO;
import ru.t1.semikolenov.tm.dto.model.AbstractModelDTO;

public interface IServiceDTO<M extends AbstractModelDTO> extends IRepositoryDTO<M> {
}

