package ru.t1.semikolenov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.service.IConnectionService;
import ru.t1.semikolenov.tm.api.service.dto.IProjectTaskServiceDTO;
import ru.t1.semikolenov.tm.api.service.dto.ITaskServiceDTO;
import ru.t1.semikolenov.tm.dto.model.TaskDTO;
import ru.t1.semikolenov.tm.exception.entity.TaskNotFoundException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.exception.field.EmptyUserIdException;

import java.util.List;

public class ProjectTaskServiceDTO implements IProjectTaskServiceDTO {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    private final ITaskServiceDTO taskService;

    public ProjectTaskServiceDTO(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
        this.taskService = new TaskServiceDTO(connectionService);
    }

    @Override
    public void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskService.update(task);
    }

    @Override
    public void unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        taskService.update(task);
    }

    @Override
    public void removeProjectById(@NotNull String userId, @NotNull String projectId) {
        @NotNull final List<TaskDTO> taskList = taskService.findAllByProjectId(userId, projectId);
        taskList.forEach(taskService::remove);
    }

}
