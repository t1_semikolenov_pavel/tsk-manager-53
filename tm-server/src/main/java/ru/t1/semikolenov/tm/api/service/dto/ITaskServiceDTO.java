package ru.t1.semikolenov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskServiceDTO extends IUserOwnedServiceDTO<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}
