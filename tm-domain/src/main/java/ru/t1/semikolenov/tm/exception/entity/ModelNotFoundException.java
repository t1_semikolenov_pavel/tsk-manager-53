package ru.t1.semikolenov.tm.exception.entity;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class ModelNotFoundException extends AbstractException {

    public ModelNotFoundException() {
        super("Error! Model not found...");
    }

}